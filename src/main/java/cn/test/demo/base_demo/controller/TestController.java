package cn.test.demo.base_demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 王杨帅
 * @create 2018-04-18 9:29
 * @desc 测试控制层
 **/
@RestController
@RequestMapping(value = "/test")
@Slf4j
public class TestController {

    @GetMapping(value = "/connect")
    public String connect() {
        String result = "测试前后台连接_wys";
        log.info("===/" + getClass().getName() + "/connect==={}", result);
        return result;
    }

}

