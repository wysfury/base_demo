package cn.test.demo.base_demo.controller;

import cn.test.demo.base_demo.entity.PO.UserPO;
import cn.test.demo.base_demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-05-09 11:25
 * @desc 用户控制层
 **/
@RestController
@Slf4j
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserPO> queryList() {
        return userService.queryList();
    }
}

