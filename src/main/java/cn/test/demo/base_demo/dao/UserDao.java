package cn.test.demo.base_demo.dao;

import cn.test.demo.base_demo.entity.PO.UserPO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author 王杨帅
 * @create 2018-05-09 11:19
 * @desc 用户持久层接口
 **/
public interface UserDao extends JpaRepository<UserPO, Integer> {
}
