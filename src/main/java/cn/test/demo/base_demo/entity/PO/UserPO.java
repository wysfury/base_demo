package cn.test.demo.base_demo.entity.PO;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author 王杨帅
 * @create 2018-05-09 11:13
 * @desc 用户实体类
 **/
@Entity
@Table(name = "t_user")
@Data
public class UserPO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;
    private String userName;
    private Integer credits;
    private String password;
    private Date lastVisit;
    private String lastIp;
}

