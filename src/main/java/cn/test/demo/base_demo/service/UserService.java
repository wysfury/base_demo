package cn.test.demo.base_demo.service;

import cn.test.demo.base_demo.entity.PO.UserPO;

import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-05-09 11:19
 * @desc 用户服务接口
 **/
public interface UserService {
    public List<UserPO> queryList();
}
