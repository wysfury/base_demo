package cn.test.demo.base_demo.service.impl;

import cn.test.demo.base_demo.dao.UserDao;
import cn.test.demo.base_demo.entity.PO.UserPO;
import cn.test.demo.base_demo.service.UserService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.ibatis.reflection.ArrayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 王杨帅
 * @create 2018-05-09 11:21
 * @desc 用户服务层实现类
 **/
@Service(value = "userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<UserPO> queryList() {
        List<UserPO> userPOList = userDao.findAll();
        if (userPOList.isEmpty()) {
//            TODO: 抛出提示信息
        }
        return userPOList;
    }
}

